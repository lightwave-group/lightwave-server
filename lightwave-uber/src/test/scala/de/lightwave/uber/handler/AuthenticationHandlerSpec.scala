package de.lightwave.uber.handler

import akka.actor.{ActorRef, ActorSystem}
import akka.io.Tcp.{Close, Write}
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import de.lightwave.frontend.io.tcp.HabboConnectionHandler.{GetPlayerInformation, SetPlayerInformation}
import de.lightwave.players.PlayerService.GetPlayer
import de.lightwave.players.model.Player
import de.lightwave.uber.io.protocol.messages._
import org.scalatest.{BeforeAndAfterAll, FunSuiteLike}

class AuthenticationHandlerSpec extends TestKit(ActorSystem("test-system", ConfigFactory.empty))
  with FunSuiteLike
  with DefaultTimeout
  with ImplicitSender
  with BeforeAndAfterAll {

  test("Log in using valid sso ticket") {
    withActor() { (handler, playerService) =>
      val player = Player(None, "test")
      handler ! SSOTicketMessage("valid")

      playerService.expectMsg(GetPlayer(1))
      playerService.reply(Some(player))

      expectMsg(SetPlayerInformation(player))
      expectMsg(Write(AuthenticationOKMessageComposer.compose))
      expectMsg(Write(OpenFlatConnectionMessageComposer.compose(1))) // Test
    }
  }

  test("Kick client when trying to log in using an invalid sso ticket") {
    withActor() { (handler, playerService) =>
      handler ! SSOTicketMessage("invalid")

      playerService.expectMsg(GetPlayer(1))
      playerService.reply(None)

      expectMsg(Close)
    }
  }

  test("Get player information") {
    withActor() { (handler, connection) =>
      val player = Player(None, "test")
      handler.tell(GetPlayerInformationMessage, connection.ref)

      connection.expectMsg(GetPlayerInformation)
      connection.reply(Some(player))

      connection.expectMsg(Write(PlayerInformationMessageComposer.compose(player)))
    }
  }

  test("Do not get player information as unauthenticated client") {
    withActor() { (handler, connection) =>
      handler.tell(GetPlayerInformationMessage, connection.ref)

      connection.expectMsg(GetPlayerInformation)
      connection.reply(None)

      connection.expectNoMsg()
    }
  }

  private def withActor()(testCode: (ActorRef, TestProbe) => Any) = {
    val playerService = TestProbe()

    testCode(system.actorOf(AuthenticationHandler.props(playerService.ref)), playerService)
  }
}

