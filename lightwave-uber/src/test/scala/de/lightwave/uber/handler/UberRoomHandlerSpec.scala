package de.lightwave.uber.handler

import akka.actor.ActorSystem
import akka.io.Tcp.Write
import akka.testkit.{DefaultTimeout, ImplicitSender, TestActorRef, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import de.lightwave.rooms.engine.entity.EntityDirector.SpawnEntity
import de.lightwave.rooms.engine.entity.RoomEntity.WalkTo
import de.lightwave.rooms.engine.entity.{EntityReference, RoomEntity}
import de.lightwave.rooms.engine.mapping.MapCoordinator.GetAbsoluteHeightMap
import de.lightwave.rooms.engine.mapping.{Vector2, Vector3}
import de.lightwave.services.pubsub.Broadcaster.Subscribe
import de.lightwave.uber.io.protocol.messages._
import org.scalatest.{BeforeAndAfterAll, FunSuiteLike}

class UberRoomHandlerSpec extends TestKit(ActorSystem("test-system", ConfigFactory.empty))
  with FunSuiteLike
  with DefaultTimeout
  with ImplicitSender
  with BeforeAndAfterAll {

  test("Spawn entity") {
    withActor(handleStartup = false) { (handler, connection, engine) =>
      engine.expectMsg(SpawnEntity(UberRoomHandlerSpec.TestReference))
    }
  }

  test("Subscribe to broadcaster of room") {
    withActor(handleStartup = false) { (handler, connection, engine) =>
      engine.expectMsg(SpawnEntity(UberRoomHandlerSpec.TestReference))
      engine.expectMsg(Subscribe(handler))
    }
  }

  test("Get furni campaigns") {
    withActor() { (handler, connection, _) =>
      handler ! GetFurniCampaignsMessage
      connection.expectMsg(Write(FurniCampaignsMessageComposer.compose))
    }
  }

  test("Get heightmap") {
    withActor() { (handler, connection, engine) =>
      val map = IndexedSeq(IndexedSeq(None))
      handler ! GetHeightmapMessage

      engine.expectMsg(GetAbsoluteHeightMap)
      engine.reply(map)

      connection.expectMsg(Write(HeightmapMessageComposer.compose(map) ++ RelativeHeightmapMessageComposer.compose(map)))
    }
  }

  test("Get room objects") {
    withActor() { (handler, connection, engine) =>
      handler ! GetRoomObjectsMessage

      connection.expectMsg(Write(FloorItemsMessageComposer.compose() ++ WallItemsMessageComposer.compose() ++ RoomInfoRightsComposer.compose(NavigatorHandler.TestRoom.id.getOrElse(0))))

      val renderInfo = RoomEntity.RenderInformation(1, UberRoomHandlerSpec.TestReference, new Vector3(1, 1, 0), RoomEntity.DefaultStance)

      engine.expectMsg(RoomEntity.GetRenderInformation)
      engine.reply(renderInfo)

      connection.expectMsg(Write(EntityListMessageComposer.compose(
        Seq((renderInfo.virtualId, renderInfo.reference, renderInfo.position))
      ) ++ EntityStanceMessageComposer.compose(renderInfo.virtualId, renderInfo.position, renderInfo.stance)))
    }
  }

  test("Display new entity on spawn event") {
    withActor() { (handler, connection, _) =>
      handler ! RoomEntity.Spawned(1, UberRoomHandlerSpec.TestReference, TestProbe().ref)
      connection.expectMsg(Write(EntityListMessageComposer.compose(Seq((1, UberRoomHandlerSpec.TestReference, new Vector3(0, 0, 0))))))
    }
  }

  test("Update entity stance on position update event") {
    withActor() { (handler, connection, _) =>
      handler ! RoomEntity.PositionUpdated(1, new Vector3(1, 1, 1), RoomEntity.DefaultStance)
      connection.expectMsg(Write(EntityStanceMessageComposer.compose(1, new Vector3(1, 1, 1), RoomEntity.DefaultStance)))
    }
  }

  test("Move entity to specific position") {
    withActor() { (handler, connection, engine) =>
      val entityProbe = TestProbe()
      handler.underlyingActor.entity = Some(entityProbe.ref)

      handler ! MoveUserMessage(Vector2(1, 1))
      entityProbe.expectMsg(WalkTo(Vector2(1, 1)))
    }
  }

  test("Remove entity from room when despawned") {
    withActor() { (handler, connection, engine) =>
      handler ! RoomEntity.Despawned(1)
      connection.expectMsg(Write(EntityRemovedMessageComposer.compose(1)))
    }
  }

  private def withActor(handleStartup: Boolean = true)(testCode: (TestActorRef[UberRoomHandler], TestProbe, TestProbe) => Any) = {
    val connection = TestProbe()
    val roomEngine = TestProbe()

    val handler = TestActorRef[UberRoomHandler](UberRoomHandler.props()(connection.ref, Some(UberRoomHandlerSpec.TestReference), roomEngine.ref))

    if (handleStartup) {
      roomEngine.expectMsg(SpawnEntity(UberRoomHandlerSpec.TestReference))
      roomEngine.expectMsg(Subscribe(handler))
    }

    testCode(handler, connection, roomEngine)
  }
}

object UberRoomHandlerSpec {
  val TestReference = EntityReference(1, "Test")
}