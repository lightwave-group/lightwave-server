package de.lightwave.uber.handler

import akka.actor.{ActorRef, ActorSystem}
import akka.io.Tcp.Write
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit}
import com.typesafe.config.ConfigFactory
import de.lightwave.uber.io.protocol.messages.{InitCryptoMessage, SessionParametersMessageComposer}
import org.scalatest.{BeforeAndAfterAll, FunSuiteLike}

class HandshakeHandlerSpec extends TestKit(ActorSystem("test-system", ConfigFactory.empty))
  with FunSuiteLike
  with DefaultTimeout
  with ImplicitSender
  with BeforeAndAfterAll {

  test("Send session parameters on init crypto message") {
    withActor() { handler =>
      handler ! InitCryptoMessage
      expectMsg(Write(SessionParametersMessageComposer.compose()))
    }
  }

  private def withActor()(testCode: ActorRef => Any) = {
    testCode(system.actorOf(HandshakeHandler.props()))
  }
}
