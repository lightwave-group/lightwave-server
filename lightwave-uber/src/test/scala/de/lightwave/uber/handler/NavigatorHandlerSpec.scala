package de.lightwave.uber.handler

import akka.actor.{ActorRef, ActorSystem}
import akka.io.Tcp.Write
import akka.testkit.{DefaultTimeout, ImplicitSender, TestKit, TestProbe}
import com.typesafe.config.ConfigFactory
import de.lightwave.frontend.io.tcp.HabboConnectionHandler.EnterRoom
import de.lightwave.rooms.engine.RoomEngine.{AlreadyInitialized, InitializeRoom}
import de.lightwave.uber.io.protocol.messages.{InitiateRoomLoadingMessageComposer, OpenFlatConnectionMessage, RoomReadyMessageComposer, SessionParametersMessageComposer}
import org.scalatest.{BeforeAndAfterAll, FunSuiteLike}

class NavigatorHandlerSpec extends TestKit(ActorSystem("test-system", ConfigFactory.empty))
  with FunSuiteLike
  with DefaultTimeout
  with ImplicitSender
  with BeforeAndAfterAll {

  test("Open flat connection") {
    withActor() { (handler, _, region) =>
      handler ! OpenFlatConnectionMessage(1)

      expectMsg(Write(InitiateRoomLoadingMessageComposer.compose))

      region.expectMsg(InitializeRoom(NavigatorHandler.TestRoom))
      region.reply(AlreadyInitialized)

      expectMsg(EnterRoom(region.ref))
      expectMsg(Write(RoomReadyMessageComposer.compose(
        NavigatorHandler.TestRoom.modelId.getOrElse("model_a"), NavigatorHandler.TestRoom.id.getOrElse(1)
      )))
    }
  }

  private def withActor()(testCode: (ActorRef, TestProbe, TestProbe) => Any) = {
    val roomService = TestProbe()
    val roomRegion = TestProbe()

    testCode(system.actorOf(NavigatorHandler.props(roomService.ref, roomRegion.ref)),
      roomService, roomRegion)
  }
}
