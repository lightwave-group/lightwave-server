package de.lightwave.uber.io

import java.net.InetSocketAddress

import akka.actor.ActorSystem
import akka.io.Tcp.{Close, Write}
import akka.testkit.{DefaultTimeout, ImplicitSender, TestActorRef, TestKit, TestProbe}
import akka.util.ByteString
import com.typesafe.config.ConfigFactory
import de.lightwave.frontend.handler.MessageHandler.HandleMessage
import de.lightwave.frontend.io.tcp.ConnectionHandler.{MessageRead, ReadMessage}
import de.lightwave.frontend.io.tcp.HabboConnectionHandler.{GetPlayerInformation, SetPlayerInformation}
import de.lightwave.frontend.io.tcp.protocol.MessageHeader
import de.lightwave.players.model.Player
import de.lightwave.uber.io.protocol.OperationCode
import de.lightwave.uber.io.protocol.messages.InitCryptoMessage
import org.scalatest.{BeforeAndAfterAll, FunSuiteLike}

class UberConnectionHandlerSpec extends TestKit(ActorSystem("test-system", ConfigFactory.empty))
  with FunSuiteLike
  with DefaultTimeout
  with ImplicitSender
  with BeforeAndAfterAll {

  test("Read message in a single chunk") {
    withActor() { (handler, _, _) =>
      handler ! ReadMessage(ByteString("@@B@A"))
      expectMsg(MessageRead(MessageHeader(0, 1), ByteString("")))
    }
  }

  test("Read message in multiple chunks") {
    withActor() { (handler, _, _) =>
      handler ! ReadMessage(ByteString("@@E@B"))
      expectNoMsg()

      handler ! ReadMessage(ByteString("AB"))
      expectNoMsg()

      handler ! ReadMessage(ByteString("C"))
      expectMsg(MessageRead(MessageHeader(3, 2), ByteString("ABC")))
    }
  }

  test("Respond to crossdomain request and disconnect") {
    withActor() { (handler, connection, messageHandler) =>
      handler ! ReadMessage(ByteString("<"))
      connection.expectMsg(Write(ByteString.fromString(UberConnectionHandler.CrossdomainFile + "\0")))
      connection.expectMsg(Close)
    }
  }

  test("Do not respond to crossdomain request if another message has already been read") {
    withActor() { (handler, _, _) =>
      handler ! ReadMessage(ByteString("@@B@A"))
      expectMsg(MessageRead(MessageHeader(0, 1), ByteString("")))

      handler ! ReadMessage(ByteString("<"))
      expectNoMsg()
    }
  }

  test("Forward messages to message handler") {
    withActor() { (handler, _, messageHandler) =>
      handler ! MessageRead(MessageHeader(0, OperationCode.Incoming.InitCrypto), ByteString.empty)
      messageHandler.expectMsg(HandleMessage(InitCryptoMessage, authenticated = false))
    }
  }

  test("Set player information") {
    withActor() { (handler, _, _) =>
      val player = Player(None, "")

      handler ! SetPlayerInformation(player)
      assert(handler.underlyingActor.playerInformation === Some(player))
    }
  }

  test("Get player information") {
    withActor() { (handler, _, _) =>
      val player = Player(None, "")

      handler ! GetPlayerInformation
      expectMsg(None)

      handler ! SetPlayerInformation(player)
      handler ! GetPlayerInformation
      expectMsg(Some(player))
    }
  }

  private def withActor()(testCode: (TestActorRef[UberConnectionHandler], TestProbe, TestProbe) => Any) = {
    val connectionProbe = TestProbe()
    val messageHandlerProbe = TestProbe()

    testCode(TestActorRef[UberConnectionHandler](
      UberConnectionHandler.props(messageHandlerProbe.ref)(new InetSocketAddress("127.0.0.1", 8124), connectionProbe.ref)
    ), connectionProbe, messageHandlerProbe)
  }

  override def afterAll: Unit = {
    shutdown()
  }
}