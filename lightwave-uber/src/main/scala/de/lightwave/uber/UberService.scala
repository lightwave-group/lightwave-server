package de.lightwave.uber

import java.net.InetSocketAddress

import akka.actor.{ActorLogging, ActorRef, Props}
import de.lightwave.frontend.io.tcp.TcpServer
import de.lightwave.services.Service
import de.lightwave.uber.handler.UberMessageHandler
import de.lightwave.uber.io.UberConnectionHandler

/**
  * Front-end server which provides game access to Habbo flash
  * clients of version r63
  */
class UberService(endpoint: InetSocketAddress, playerService: ActorRef, roomRegion: ActorRef) extends Service with ActorLogging {
  val messageHandler: ActorRef = context.actorOf(UberMessageHandler.props(playerService, roomRegion), "MessageHandler")
  val tcpServer: ActorRef = context.actorOf(TcpServer.props(endpoint, UberConnectionHandler.props(messageHandler)))

  override def receive: Receive = {
    case _ =>
  }
}

object UberService {
  def props(endpoint: InetSocketAddress, playerService: ActorRef, roomRegion: ActorRef): Props =
    Props(classOf[UberService], endpoint, playerService, roomRegion)
}
