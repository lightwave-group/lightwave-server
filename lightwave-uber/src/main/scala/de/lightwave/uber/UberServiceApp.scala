package de.lightwave.uber

import java.net.InetSocketAddress

import akka.actor.ActorSystem
import akka.cluster.sharding.ClusterSharding
import com.typesafe.config.Config
import de.lightwave.dedicated.commands.DedicatedServerCommandHandler
import de.lightwave.players.PlayerServiceApp
import de.lightwave.rooms.RoomServiceApp
import de.lightwave.rooms.engine.RoomEngine
import de.lightwave.services.{ServiceApp, ServiceGroups}

object UberServiceApp extends ServiceApp {
  val ConfigEndpointHostPath = "lightwave.uber.endpoint.host"
  val ConfigEndpointPortPath = "lightwave.uber.endpoint.port"

  val serviceName = "ShockwaveService"
  val role = Some("shockwave")

  override def onStart(config: Config, system: ActorSystem, commandHandler: DedicatedServerCommandHandler): Unit = {
    val roomRegion = ClusterSharding(system).startProxy(
      typeName = RoomEngine.shardName,
      role = RoomServiceApp.role,
      extractEntityId = RoomEngine.extractEntityId,
      extractShardId = RoomEngine.extractShardId)

    val playerService = ServiceGroups.createGroup(system, PlayerServiceApp)

    system.actorOf(UberService.props(
      new InetSocketAddress(config.getString(ConfigEndpointHostPath), config.getInt(ConfigEndpointPortPath)),
      playerService,
      roomRegion
    ), serviceName)
  }
}
