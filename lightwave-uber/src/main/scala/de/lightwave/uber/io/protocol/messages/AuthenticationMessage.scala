package de.lightwave.uber.io.protocol.messages

import akka.util.ByteString
import de.lightwave.frontend.io.tcp.protocol.alpha._
import de.lightwave.players.model.Player
import de.lightwave.uber.io.protocol.{OperationCode, UberMessage}

trait AuthenticationMessage extends UberMessage

/**
  * Login using sso ticket
  */
case class SSOTicketMessage(ssoTicket: String) extends AuthenticationMessage

object SSOTicketMessageParser extends AlphaMessageParser[SSOTicketMessage] {
  val opCode = OperationCode.Incoming.SSOTicket
  def parse(reader: AlphaMessageReader) = SSOTicketMessage(reader.readString)
}

/**
  * Request player information
  */
case object GetPlayerInformationMessage extends AuthenticationMessage

object GetPlayerInformationMessageParser extends AlphaMessageParser[GetPlayerInformationMessage.type] {
  val opCode = OperationCode.Incoming.GetPlayerInfo
  def parse(reader: AlphaMessageReader) = GetPlayerInformationMessage
}

/**
  * Grant access to user
  */
object AuthenticationOKMessageComposer extends AlphaMessageComposer {
  def compose: ByteString = init(OperationCode.Outgoing.AuthenticationOK).toByteString
}

/**
  * Player object
  */
object PlayerInformationMessageComposer extends AlphaMessageComposer {
  def compose(player: Player): ByteString = init(OperationCode.Outgoing.PlayerInformation)
    .push(player.id.getOrElse(0).toString)
    .push(player.nickname)
    .push("") // avatar
    .push("M")
    .push("test")
    .push(player.nickname)
    .push(false)
    .push(0) // respect points
    .push(3) // available respects
    .push(3) // available scratches
    .push(false) // stream enabled
    .toByteString
}