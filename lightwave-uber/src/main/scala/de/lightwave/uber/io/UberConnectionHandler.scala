package de.lightwave.uber.io

import java.net.InetSocketAddress

import akka.actor.{ActorRef, Props}
import akka.util.ByteString
import de.lightwave.frontend.io.tcp.{ConnectionHandler, HabboConnectionHandler}
import de.lightwave.frontend.io.tcp.protocol.alpha.AlphaMessageHeader
import de.lightwave.uber.handler.UberRoomHandler
import de.lightwave.uber.io.protocol.UberMessageParser
import de.lightwave.uber.io.protocol.messages.RoomMessage

/**
  * Handler of clients that are connected to the uber server.
  */
class UberConnectionHandler(remoteAddress: InetSocketAddress, connection: ActorRef, messageHandler: ActorRef)
  extends HabboConnectionHandler(remoteAddress, connection, messageHandler, AlphaMessageHeader, UberMessageParser, UberRoomHandler.props()) {

  import akka.io.Tcp._

  // First message should be a crossdomain request here, if it's not,
  // the real handling may begin
  var crossdomainRequestIncoming = true

  override def handleMessage(msg: Any): Unit = msg match {
    case e:RoomMessage => handleRoomMessage(e)
    case _ => super.handleMessage(msg)
  }

  /**
    * Crossdomain file request handling
    */
  override def readMessage(data: ByteString, recipient: ActorRef): Unit = if (crossdomainRequestIncoming && data.utf8String.charAt(0) == '<') {
    // Send crossdomain file and disconnect
    connection ! Write(ByteString.fromString(UberConnectionHandler.CrossdomainFile + "\0"))
    connection ! Close
  } else {
    crossdomainRequestIncoming = false
    super.readMessage(data, recipient)
  }
}

object UberConnectionHandler {
  val CrossdomainFile: String = "<?xml version=\"1.0\"?>\r\n" + "<!DOCTYPE cross-domain-policy SYSTEM \"/xml/dtds/cross-domain-policy.dtd\">\r\n" + "<cross-domain-policy>\r\n" + "<allow-access-from domain=\"*\" to-ports=\"*\" />\r\n" + "</cross-domain-policy>"

  def props(messageHandler: ActorRef)(remoteAddress: InetSocketAddress, connection: ActorRef) =
    Props(classOf[UberConnectionHandler], remoteAddress, connection, messageHandler)
}