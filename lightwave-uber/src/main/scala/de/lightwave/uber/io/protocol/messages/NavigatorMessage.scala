package de.lightwave.uber.io.protocol.messages

import akka.util.ByteString
import de.lightwave.frontend.io.tcp.protocol.alpha._
import de.lightwave.rooms.model.Rooms.RoomId
import de.lightwave.uber.io.protocol.{OperationCode, UberMessage}

trait NavigatorMessage extends UberMessage

/**
  * Open connection to private room
  */
case class OpenFlatConnectionMessage(roomId: RoomId, password: Option[String] = None) extends NavigatorMessage

object OpenFlatConnectionMessageParser extends AlphaMessageParser[OpenFlatConnectionMessage] {
  val opCode = OperationCode.Incoming.OpenFlatConnection
  def parse(reader: AlphaMessageReader): OpenFlatConnectionMessage = {
    val id = reader.readInt
    val password = reader.readString

    OpenFlatConnectionMessage(id, if (password.isEmpty) None else Some(password))
  }
}

/**
  * Initialize room loading on command. Used for home room move
  */
object OpenFlatConnectionMessageComposer extends AlphaMessageComposer {
  def compose(roomId: RoomId): ByteString = init(OperationCode.Outgoing.OpenFlatConnection)
    .push(roomId)
    .toByteString
}

/**
  * Marks requested room as enterable
  */
object InitiateRoomLoadingMessageComposer extends AlphaMessageComposer {
  val compose: ByteString = init(OperationCode.Outgoing.InitiateRoomLoading).toByteString
}

/**
  * Tells client that requested room has been started
  */
object RoomReadyMessageComposer extends AlphaMessageComposer {
  def compose(model: String, roomId: RoomId): ByteString = init(OperationCode.Outgoing.RoomReady)
    .push(model)
    .push(roomId)
    .toByteString
}