package de.lightwave.uber.io.protocol.messages

import akka.util.ByteString
import de.lightwave.frontend.io.tcp.protocol.alpha._
import de.lightwave.uber.io.protocol.{OperationCode, UberMessage}

trait HandshakeMessage extends UberMessage

/**
  * Requests crypto parameters
  */
case object InitCryptoMessage extends HandshakeMessage

object InitCryptoMessageParser extends AlphaMessageParser[InitCryptoMessage.type] {
  val opCode = OperationCode.Incoming.InitCrypto
  def parse(reader: AlphaMessageReader) = InitCryptoMessage
}

/**
  * Some session specific settings such as date format
  */
object SessionParametersMessageComposer extends AlphaMessageComposer {
  def compose(): ByteString = init(OperationCode.Outgoing.SessionParameters)
    .push(9)
    .push(false)
    .push(false)
    .push(true)
    .push(true)
    .push(3)
    .push(false)
    .push(2)
    .push(false)
    .push(4)
    .push(1)
    .push(5)
    .push("dd-MM-yyyy")
    .push(7)
    .push(0)
    .push(8)
    .push("http://hotel-de")
    .push(9)
    .toByteString
}

/**
  * Send alert to client
  */
object NotificationMessageComposer extends AlphaMessageComposer {
  def compose(message: String): ByteString = init(OperationCode.Outgoing.Notification)
    .push(message)
    .toByteString
}