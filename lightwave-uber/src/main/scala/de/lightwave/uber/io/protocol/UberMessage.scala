package de.lightwave.uber.io.protocol

import de.lightwave.frontend.io.tcp.protocol.alpha.AlphaMessage
import de.lightwave.frontend.io.tcp.protocol.{MessageParser, MessageParserLibrary}
import de.lightwave.uber.io.protocol.messages._

trait UberMessage extends AlphaMessage

/**
  * Collection of message parsers that are assigned
  * to certain operation codes
  */
object UberMessageParser extends MessageParserLibrary {
  private var parsers: Array[MessageParser[_]] = Array(
    InitCryptoMessageParser, SSOTicketMessageParser, GetPlayerInformationMessageParser, OpenFlatConnectionMessageParser,
    GetFurniCampaignsMessageParser, GetHeightmapMessageParser, GetRoomObjectsMessageParser, MoveUserMessageParser,
    QuietChatMessageParser
  )

  private var parsersByOpCode: Map[Short, MessageParser[_]] =
    parsers.flatMap(parser => parser.opCodes.map(_ -> parser)).toMap

  def get(opCode: Short): Option[MessageParser[_]] = parsersByOpCode.get(opCode)
}
