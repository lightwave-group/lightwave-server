package de.lightwave.uber.io.protocol.messages

import akka.util.ByteString
import de.lightwave.frontend.io.tcp.protocol.alpha._
import de.lightwave.migration.RoomMigration
import de.lightwave.rooms.engine.entity.{EntityReference, EntityStance}
import de.lightwave.rooms.engine.mapping.RoomMap.StaticMap
import de.lightwave.rooms.engine.mapping.{Vector2, Vector3}
import de.lightwave.rooms.model.Room
import de.lightwave.uber.io.protocol.{OperationCode, UberMessage}

trait RoomMessage extends UberMessage
trait RoomUserMessage extends RoomMessage

case object GetFurniCampaignsMessage extends RoomMessage

object GetFurniCampaignsMessageParser extends AlphaMessageParser[GetFurniCampaignsMessage.type] {
  val opCode = OperationCode.Incoming.GetFurniCampaigns
  def parse(reader: AlphaMessageReader) = GetFurniCampaignsMessage
}

case object GetHeightmapMessage extends RoomMessage

object GetHeightmapMessageParser extends AlphaMessageParser[GetHeightmapMessage.type] {
  val opCode = OperationCode.Incoming.GetHeightmap
  def parse(reader: AlphaMessageReader) = GetHeightmapMessage
}

case object GetRoomObjectsMessage extends RoomMessage

object GetRoomObjectsMessageParser extends AlphaMessageParser[GetRoomObjectsMessage.type] {
  val opCode = OperationCode.Incoming.GetRoomObjects
  def parse(reader: AlphaMessageReader) = GetRoomObjectsMessage
}

/**
  * Move user in room to specific tile
  */
case class MoveUserMessage(pos: Vector2) extends RoomUserMessage

object MoveUserMessageParser extends AlphaMessageParser[MoveUserMessage] {
  val opCode = OperationCode.Incoming.MoveUser

  override def parse(reader: AlphaMessageReader) = MoveUserMessage(
    new Vector2(reader.readInt, reader.readInt)
  )
}

case class QuietChatMessage(message: String) extends RoomUserMessage

object QuietChatMessageParser extends AlphaMessageParser[QuietChatMessage] {
  val opCode = OperationCode.Incoming.QuietChat

  override def parse(reader: AlphaMessageReader) = QuietChatMessage(reader.readString)
}

object FurniCampaignsMessageComposer extends AlphaMessageComposer {
  def compose: ByteString = init(OperationCode.Outgoing.FurniCampaigns).toByteString
}

/**
  * Send heightmap of room to client
  */
object HeightmapMessageComposer extends AlphaMessageComposer {
  def compose(heightmap: StaticMap[Double]): ByteString = init(OperationCode.Outgoing.Heightmap)
    .push(ByteString.fromString(RoomMigration.composeMap(heightmap)))
    .toByteString
}

object RelativeHeightmapMessageComposer extends AlphaMessageComposer {
  def compose(heightmap: StaticMap[Double]): ByteString = init(OperationCode.Outgoing.RelativeHeightmap)
    .push(ByteString.fromString(RoomMigration.composeMap(heightmap)))
    .toByteString
}

/**
  * TODO: Send floor items to client
  */
object FloorItemsMessageComposer extends AlphaMessageComposer {
  def compose(): ByteString = init(OperationCode.Outgoing.FloorItems).push(0).toByteString
}

/**
  * TODO: Send wall items to client
  */
object WallItemsMessageComposer extends AlphaMessageComposer {
  def compose(): ByteString = init(OperationCode.Outgoing.WallItems).push(0).toByteString
}

object RoomInformationMessageComposer extends AlphaMessageComposer {
  def compose(room: Room): ByteString = init(OperationCode.Outgoing.RoomInformation)
    .push(true) // is loading
    .push(room.id.getOrElse(1))
    .push(0)
    .push(room.name)
    .push("Steve")
    .push(0) // access type
    .push(0) // total players
    .push(20) // max players
    .push(room.description)
    .push(0)
    .push(false) // trading enabled
    .push(0)
    .push(0) // category id
    .push("")
    .push(0) // tags
    .push(0) // background
    .push(0) // overlay
    .push(0) // objects
    .push(false) // pets allowed?
    .push(true)
    .push(false) // check entry?
    .push(false) // staff picked
    .toByteString
}

/**
  * Display entities that are currently in the room
  * TODO: Add figure, mission, gender
  */
object EntityListMessageComposer extends AlphaMessageComposer {
  def compose(entities: Seq[(Int, EntityReference, Vector3)]): ByteString = {
    val msg = init(OperationCode.Outgoing.EntityList).push(entities.size)
    entities.foreach {
      case (virtualId, EntityReference(id, name), Vector3(x, y, z)) =>
        msg
          .push(id)
          .push(name)
          .push("Test mission")
          .push("ch-215-104.hr-100-37.hd-180-7.lg-275-110")
          .push(virtualId)
          .push(x)
          .push(y)
          .push(z.toString)
          .push(2)
          .push(1)
          .push("m")
          .push(-1)
          .push(-1)
          .push(-1)
          .push("")
          .push(0) // score
    }
    msg.toByteString
  }
}

object RoomInfoRightsComposer extends AlphaMessageComposer {
  def compose(roomId: Int): ByteString = init(OperationCode.Outgoing.RoomInfoRights)
    .push(true)
    .push(roomId)
    .push(true)
    .toByteString
}

/**
  * Update stance of a specific entity
  */
object EntityStanceMessageComposer extends AlphaMessageComposer {
  def compose(virtualId: Int, pos: Vector3, stance: EntityStance): ByteString = init(OperationCode.Outgoing.EntityStance)
    .push(1)
    .push(virtualId)
    .push(pos.x)
    .push(pos.y)
    .push(pos.z.toString)
    .push(RoomMigration.convertDirection(stance.headDirection))
    .push(RoomMigration.convertDirection(stance.bodyDirection))
    .push(s"/${RoomMigration.composeEntityStatuses(stance.properties)}/")
    .toByteString
}

/**
  * Removes an entity from a room
  */
object EntityRemovedMessageComposer extends AlphaMessageComposer {
  def compose(entityId: Int): ByteString = init(OperationCode.Outgoing.EntityRemoved)
    .push(entityId.toString)
    .toByteString
}

/**
  * Send quiet chat message to people in room
  */
object QuietChatMessageComposer extends AlphaMessageComposer {
  def compose(entityId: Int, message: String): ByteString = init(OperationCode.Outgoing.QuietChat)
    .push(entityId)
    .push(message)
    .push(0) // emotion id
    .push(0) // links
    .push(0)
    .toByteString
}