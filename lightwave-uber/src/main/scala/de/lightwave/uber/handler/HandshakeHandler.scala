package de.lightwave.uber.handler

import akka.actor.{Actor, ActorLogging, Props}
import akka.io.Tcp.Write
import de.lightwave.uber.io.protocol.messages._

class HandshakeHandler extends Actor with ActorLogging {
  override def receive = {
    case InitCryptoMessage => sender() ! Write(SessionParametersMessageComposer.compose())
  }
}

object HandshakeHandler {
  def props() = Props(classOf[HandshakeHandler])
}
