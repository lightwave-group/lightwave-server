package de.lightwave.uber.handler

import akka.actor.{ActorRef, Props}
import akka.io.Tcp.Write
import de.lightwave.frontend.handler.RoomHandler
import de.lightwave.rooms.engine.entity.{EntityReference, RoomEntity}
import de.lightwave.rooms.engine.entity.RoomEntity.{SendChatMessage, WalkTo}
import de.lightwave.rooms.engine.mapping.MapCoordinator.GetAbsoluteHeightMap
import de.lightwave.rooms.engine.mapping.RoomMap.StaticMap
import de.lightwave.rooms.engine.mapping.Vector3
import de.lightwave.uber.io.protocol.messages._

class UberRoomHandler(connection: ActorRef, entityReference: Option[EntityReference], roomEngine: ActorRef)
  extends RoomHandler(connection, entityReference, roomEngine) {

  import context.dispatcher
  import akka.pattern._

  override def postStop(): Unit = {
    super.postStop()

    if (errorMessageOnTermination) {
      connection ! Write(NotificationMessageComposer.compose("The room has been closed due to an error."))
    }
  }

  override def entityReceive(entity: ActorRef): Receive = {
    case MoveUserMessage(pos) => entity ! WalkTo(pos)
    case QuietChatMessage(msg) =>
      // TODO: Filter
      entity ! SendChatMessage(msg)
  }

  def messageReceive: Receive = {
    case GetFurniCampaignsMessage => connection ! Write(FurniCampaignsMessageComposer.compose)
    case GetHeightmapMessage => (roomEngine ? GetAbsoluteHeightMap).map {
      case map:IndexedSeq[_] =>
        val heightmap = map.asInstanceOf[StaticMap[Double]]
        Write(HeightmapMessageComposer.compose(heightmap) ++ RelativeHeightmapMessageComposer.compose(heightmap))
    }.recover {
      case _ => Write(HeightmapMessageComposer.compose(IndexedSeq.empty))
    } pipeTo connection
    case GetRoomObjectsMessage =>
      connection ! Write(FloorItemsMessageComposer.compose() ++ WallItemsMessageComposer.compose() ++ RoomInfoRightsComposer.compose(NavigatorHandler.TestRoom.id.getOrElse(0)))
      roomEngine ! RoomEntity.GetRenderInformation
    case msg:RoomUserMessage => handleRoomUserMessage(msg)
  }

  def eventReceive: Receive = {
    case RoomEntity.Spawned(id, reference, _) =>
      connection ! Write(EntityListMessageComposer.compose(Seq((id, reference, Vector3.empty))))
    case RoomEntity.Despawned(id) =>
      connection ! Write(EntityRemovedMessageComposer.compose(id))
    case RoomEntity.PositionUpdated(id, pos, stance) =>
      connection ! Write(EntityStanceMessageComposer.compose(id, pos, stance))
    case RoomEntity.ChatMessageSent(id, msg) =>
      connection ! Write(QuietChatMessageComposer.compose(id, msg))
  }

  override def receive: Receive = super.receive orElse messageReceive orElse eventReceive orElse {
    case RoomEntity.RenderInformation(id, reference, pos, stance) =>
      connection ! Write(EntityListMessageComposer.compose(Seq((id, reference, pos))) ++ EntityStanceMessageComposer.compose(id, pos, stance))
  }
}

object UberRoomHandler {
  def props()(connection: ActorRef, entityReference: Option[EntityReference], roomEngine: ActorRef) = Props(
    classOf[UberRoomHandler], connection, entityReference, roomEngine
  )
}