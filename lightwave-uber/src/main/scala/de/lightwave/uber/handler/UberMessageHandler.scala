package de.lightwave.uber.handler

import akka.actor.{Actor, ActorRef, Props}
import de.lightwave.frontend.handler.MessageHandler
import de.lightwave.uber.io.protocol.messages.{AuthenticationMessage, HandshakeMessage, NavigatorMessage}

/**
  * Forwards client messages to handlers of a specific role
  */
class UberMessageHandler(playerService: ActorRef, roomRegion: ActorRef) extends MessageHandler {
  val handshakeHandler: ActorRef = context.actorOf(HandshakeHandler.props(), "Handshake")
  val authenticationHandler: ActorRef = context.actorOf(AuthenticationHandler.props(playerService), "Authentication")
  val navigatorHandler: ActorRef = context.actorOf(NavigatorHandler.props(Actor.noSender, roomRegion), "Navigator")

  def handleMessage(authenticated: Boolean): Receive = {
    case msg:HandshakeMessage => handshakeHandler forward msg
    case msg:AuthenticationMessage => authenticationHandler forward msg

    case msg if authenticated => msg match {
      case _:NavigatorMessage => navigatorHandler forward msg
    }
  }
}

object UberMessageHandler {
  def props(playerService: ActorRef, roomRegion: ActorRef): Props = Props(classOf[UberMessageHandler], playerService, roomRegion)
}

