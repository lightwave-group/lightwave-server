package de.lightwave.uber.handler

import akka.actor.{Actor, ActorRef, Props}
import akka.io.Tcp.Write
import de.lightwave.frontend.io.tcp.HabboConnectionHandler.EnterRoom
import de.lightwave.rooms.engine.RoomEngine.{AlreadyInitialized, InitializeRoom, Initialized}
import de.lightwave.rooms.model.Room
import de.lightwave.uber.io.protocol.messages.{InitiateRoomLoadingMessageComposer, OpenFlatConnectionMessage, RoomInformationMessageComposer, RoomReadyMessageComposer}

class NavigatorHandler(roomService: ActorRef, roomRegion: ActorRef) extends Actor {
  override def receive = {
    case OpenFlatConnectionMessage(roomId, _) =>
      val replyTo = sender()

      replyTo ! Write(InitiateRoomLoadingMessageComposer.compose)

      // Create room engine and pass it to the connection handler
      // Timeout of 5 seconds
      roomRegion.tell(InitializeRoom(NavigatorHandler.TestRoom), context.actorOf(Props(new Actor {
        import scala.concurrent.duration._
        import context.dispatcher

        override def preStart(): Unit = {
          context.system.scheduler.scheduleOnce(5.seconds, self, "")
        }

        def receive: Receive = {
          case Initialized | AlreadyInitialized =>
            replyTo ! EnterRoom(sender())
            replyTo ! Write(RoomReadyMessageComposer.compose(
              NavigatorHandler.TestRoom.modelId.getOrElse("model_a"), NavigatorHandler.TestRoom.id.getOrElse(1)
            ))
            context.stop(self)
          case _ => context.stop(self)
        }
      })))
  }
}

object NavigatorHandler {
  val TestRoom = Room(Some(1), "Test room", "Test room", Some("model_a"))

  def props(roomService: ActorRef, roomRegion: ActorRef) = Props(classOf[NavigatorHandler], roomService, roomRegion)
}
