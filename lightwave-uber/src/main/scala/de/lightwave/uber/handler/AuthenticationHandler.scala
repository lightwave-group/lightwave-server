package de.lightwave.uber.handler

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.Tcp.{Close, Write}
import akka.util.Timeout
import de.lightwave.frontend.io.tcp.HabboConnectionHandler.{GetPlayerInformation, SetPlayerInformation}
import de.lightwave.players.PlayerService.GetPlayer
import de.lightwave.players.model.Player
import de.lightwave.stats.StatsD
import de.lightwave.uber.io.protocol.messages._

class AuthenticationHandler(playerService: ActorRef) extends Actor with ActorLogging {
  import context.dispatcher
  import akka.pattern._
  import scala.concurrent.duration._

  implicit val timeout: Timeout = Timeout(5.seconds)

  override def receive = {
    case SSOTicketMessage(_) =>
      val replyTo = sender()
      val loginFuture = (playerService ? GetPlayer(1)).mapTo[Option[Player]].recover {
        case _ => None // Show invalid username/password on timeout (change it?)
      }

      if (StatsD.instance.isDefined) {
        StatsD.instance.get.increment("lightwave.authentication.attempted")
      }

      loginFuture foreach {
        case Some(player) =>
          if (StatsD.instance.isDefined) {
            StatsD.instance.get.increment("lightwave.authentication.succeeded")
          }
          loginPlayer(replyTo, player)
        case None =>
          if (StatsD.instance.isDefined) {
            StatsD.instance.get.increment("lightwave.authentication.failed")
          }
          replyTo ! Close
      }
    case GetPlayerInformationMessage =>
      val replyTo = sender()

      (replyTo ? GetPlayerInformation).mapTo[Option[Player]].recover {
        case _ => None
      }.foreach {
        case Some(player) => replyTo ! Write(PlayerInformationMessageComposer.compose(player))
        case None => log.warning("Unauthenticated client trying to fetch player information.")
      }
  }

  def loginPlayer(connection: ActorRef, player: Player): Unit = {
    connection ! SetPlayerInformation(player)
    connection ! Write(AuthenticationOKMessageComposer.compose)
    connection ! Write(OpenFlatConnectionMessageComposer.compose(1)) // Send to room 1
  }
}

object AuthenticationHandler {
  def props(playerService: ActorRef) = Props(classOf[AuthenticationHandler], playerService)
}
