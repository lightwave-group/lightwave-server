package de.lightwave.shockwave.handler

import akka.actor.{ActorRef, Props}
import akka.io.Tcp.Write
import de.lightwave.frontend.handler.RoomHandler
import de.lightwave.rooms.engine.entity.{EntityReference, RoomEntity}
import de.lightwave.rooms.engine.entity.RoomEntity.WalkTo
import de.lightwave.rooms.engine.mapping.MapCoordinator.GetAbsoluteHeightMap
import de.lightwave.rooms.engine.mapping.RoomMap.StaticMap
import de.lightwave.rooms.engine.mapping.Vector3
import de.lightwave.shockwave.io.protocol.messages._

class ShockwaveRoomHandler(connection: ActorRef, entityReference: Option[EntityReference], roomEngine: ActorRef)
  extends RoomHandler(connection, entityReference, roomEngine) {

  import context.dispatcher
  import akka.pattern._

  override def postStop(): Unit = {
    super.postStop()

    if (errorMessageOnTermination) {
      connection ! Write(NotificationMessageComposer.compose("The room has been closed due to an error."))
    }
  }

  override def entityReceive(entity: ActorRef): Receive = {
    case MoveUserMessage(pos) => entity ! WalkTo(pos)
  }

  def messageReceive: Receive = {
    case GetHeightmapMessage => (roomEngine ? GetAbsoluteHeightMap).map {
      case map:IndexedSeq[_] => Write(HeightmapMessageComposer.compose(map.asInstanceOf[StaticMap[Double]]))
    }.recover {
      case _ => Write(HeightmapMessageComposer.compose(IndexedSeq.empty))
    } pipeTo connection
    case GetUsersMessage =>
      connection ! Write(EntityListMessageComposer.compose(Seq.empty))
      roomEngine ! RoomEntity.GetRenderInformation
    case GetObjectsMessage =>
      connection ! Write(PublicObjectsMessageComposer.compose())
      connection ! Write(FloorItemsMessageComposer.compose())
    case GetItemsMessage =>
      connection ! Write(WallItemsMessageComposer.compose())
    // Re-render users to be sure
    case GetUserStancesMessage => roomEngine ! RoomEntity.GetRenderInformation
    case msg:RoomUserMessage => handleRoomUserMessage(msg)
  }

  def eventReceive: Receive = {
    case RoomEntity.Spawned(id, reference, _) =>
      connection ! Write(EntityListMessageComposer.compose(Seq((id, reference, Vector3.empty))))
    case RoomEntity.Despawned(id) =>
      connection ! Write(EntityRemovedMessageComposer.compose(id))
    case RoomEntity.PositionUpdated(id, pos, stance) =>
      connection ! Write(EntityStanceMessageComposer.compose(id, pos, stance))
  }

  override def receive: Receive = super.receive orElse messageReceive orElse eventReceive orElse {
    case RoomEntity.RenderInformation(id, reference, pos, stance) =>
      connection ! Write(EntityListMessageComposer.compose(Seq((id, reference, pos))) ++ EntityStanceMessageComposer.compose(id, pos, stance))
  }
}

object ShockwaveRoomHandler {
  def props()(connection: ActorRef, entityReference: Option[EntityReference], roomEngine: ActorRef) = Props(
    classOf[ShockwaveRoomHandler], connection, entityReference, roomEngine
  )
}