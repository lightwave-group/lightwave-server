package de.lightwave.shockwave.handler

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.io.Tcp.Write
import akka.util.Timeout
import de.lightwave.frontend.io.tcp.HabboConnectionHandler.{GetPlayerInformation, SetPlayerInformation}
import de.lightwave.players.PlayerService.AuthenticatePlayer
import de.lightwave.players.model.Player
import de.lightwave.shockwave.io.protocol.messages._
import de.lightwave.stats.StatsD

class FrontpageHandler(playerService: ActorRef) extends Actor with ActorLogging {
  import context.dispatcher
  import akka.pattern._
  import scala.concurrent.duration._

  implicit val timeout: Timeout = Timeout(5.seconds)

  override def receive = {
    case LoginMessage(username, password) =>
      val replyTo = sender()
      val loginFuture = (playerService ? AuthenticatePlayer(username, password)).mapTo[Option[Player]].recover {
        case _ => None // Show invalid username/password on timeout (change it?)
      }

      if (StatsD.instance.isDefined) {
        StatsD.instance.get.increment("lightwave.authentication.attempted")
      }

      loginFuture foreach {
        case Some(player) =>
          if (StatsD.instance.isDefined) {
            StatsD.instance.get.increment("lightwave.authentication.succeeded")
          }
          loginPlayer(replyTo, player)
        case None =>
          if (StatsD.instance.isDefined) {
            StatsD.instance.get.increment("lightwave.authentication.failed")
          }
          replyTo ! Write(LoginFailedMessageComposer.compose("Login Incorrect: Invalid username/password combination."))
      }

    case GetPlayerInformationMessage =>
      val replyTo = sender()

      (replyTo ? GetPlayerInformation).mapTo[Option[Player]].recover {
        case _ => None
      }.foreach {
        case Some(player) => replyTo ! Write(PlayerInformationMessageComposer.compose(player))
        case None => log.warning("Unauthenticated client trying to fetch player information.")
      }
  }

  def loginPlayer(connection: ActorRef, player: Player): Unit = {
    connection ! Write(AuthenticatedMessageComposer.compose)
    connection ! SetPlayerInformation(player)
  }
}

object FrontpageHandler {
  def props(playerService: ActorRef) = Props(classOf[FrontpageHandler], playerService)
}
