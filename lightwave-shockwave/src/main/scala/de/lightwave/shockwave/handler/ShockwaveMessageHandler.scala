package de.lightwave.shockwave.handler

import akka.actor.{Actor, ActorRef, Props}
import de.lightwave.frontend.handler.MessageHandler
import de.lightwave.shockwave.io.protocol.messages.{FrontpageMessage, HandshakeMessage, NavigatorMessage}

/**
  * Forwards client messages to handlers of a specific role
  */
class ShockwaveMessageHandler(playerService: ActorRef, roomRegion: ActorRef) extends MessageHandler {
  val handshakeHandler: ActorRef = context.actorOf(HandshakeHandler.props(), "Handshake")
  val frontpageHandler: ActorRef = context.actorOf(FrontpageHandler.props(playerService), "Frontpage")
  val navigatorHandler: ActorRef = context.actorOf(NavigatorHandler.props(roomRegion) ,"Navigator")

  def handleMessage(authenticated: Boolean): Receive = {
    case msg:HandshakeMessage => handshakeHandler forward msg
    case msg:FrontpageMessage => frontpageHandler forward msg

    case msg if authenticated => msg match {
      case _:NavigatorMessage => navigatorHandler forward msg
    }
  }
}

object ShockwaveMessageHandler {
  def props(playerService: ActorRef, roomRegion: ActorRef): Props = Props(classOf[ShockwaveMessageHandler], playerService, roomRegion)
}

