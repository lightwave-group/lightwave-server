package de.lightwave.shockwave.io.protocol

import akka.util.ByteString
import de.lightwave.frontend.io.tcp.protocol.alpha.AlphaMessage
import de.lightwave.frontend.io.tcp.protocol.{MessageParser, MessageParserLibrary}
import de.lightwave.shockwave.io.protocol.messages._

trait ShockwaveMessage extends AlphaMessage

/**
  * Collection of message parsers that are assigned
  * to certain operation codes
  */
object ShockwaveMessageParser extends MessageParserLibrary {
  private var parsers: Array[MessageParser[_]] = Array(
    PongMessageParser, InitCryptoMessageParser, GenerateKeyMessageParser, LoginMessageParser, GetPlayerInfoMessageParser,
    GetFlatInformationMessageParser, NavigateMessageParser, GetRecommendedRoomsMessageParser, GetLoadingAdvertisementMessageParser,
    RoomDirectoryMessageParser, TryFlatMessageParser, GoToFlatMessageParser, GetHeightmapMessageParser, GetUsersMessageParser,
    GetObjectsMessageParser, GetItemsMessageParser, GetUserStancesMessageParser, MoveUserMessageParser
  )

  private var parsersByOpCode: Map[Short, MessageParser[_]] =
    parsers.flatMap(parser => parser.opCodes.map(_ -> parser)).toMap

  def get(opCode: Short): Option[MessageParser[_]] = parsersByOpCode.get(opCode)
}