package de.lightwave.shockwave.io

import java.net.InetSocketAddress

import akka.actor.{ActorRef, Props}
import de.lightwave.frontend.io.tcp.{ConnectionHandler, HabboConnectionHandler}
import de.lightwave.frontend.io.tcp.protocol.alpha.AlphaMessageHeader
import de.lightwave.shockwave.handler.ShockwaveRoomHandler
import de.lightwave.shockwave.io.protocol.messages.{HelloMessageComposer, RoomMessage}
import de.lightwave.shockwave.io.protocol.ShockwaveMessageParser

/**
  * Handler of clients that are connected to the shockwave server.
  */
class ShockwaveConnectionHandler(remoteAddress: InetSocketAddress, connection: ActorRef, messageHandler: ActorRef)
  extends HabboConnectionHandler(remoteAddress, connection, messageHandler, AlphaMessageHeader, ShockwaveMessageParser, ShockwaveRoomHandler.props()) {

  import akka.io.Tcp._

  override def preStart(): Unit = {
    super.preStart()
    connection ! Write(HelloMessageComposer.compose())
  }

  override def handleMessage(msg: Any): Unit = msg match {
    case e:RoomMessage => handleRoomMessage(e)
    case _ => super.handleMessage(msg)
  }
}

object ShockwaveConnectionHandler {
  def props(messageHandler: ActorRef)(remoteAddress: InetSocketAddress, connection: ActorRef) =
    Props(classOf[ShockwaveConnectionHandler], remoteAddress, connection, messageHandler)
}