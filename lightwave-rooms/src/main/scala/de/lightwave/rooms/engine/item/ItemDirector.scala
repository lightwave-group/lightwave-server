package de.lightwave.rooms.engine.item

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.actor.Actor.Receive
import de.lightwave.rooms.engine.EngineComponent
import de.lightwave.rooms.engine.EngineComponent.{AlreadyInitialized, Initialize, Initialized}
import de.lightwave.rooms.engine.item.ItemDirector.SpawnItem
import de.lightwave.rooms.engine.mapping.Vector3
import de.lightwave.services.pubsub.Broadcaster.Publish

import scala.collection.mutable

class ItemDirector(mapCoordinator: ActorRef, broadcaster: ActorRef) extends EngineComponent with ActorLogging {
  val items: mutable.HashMap[Int, ActorRef] = mutable.HashMap.empty[Int, ActorRef]

  def spawnItem(itemId: Int, pos: Vector3): ActorRef = {
    val item = context.actorOf(RoomItem.props(itemId)(mapCoordinator, self, broadcaster))

    items += (itemId -> item)
    broadcaster ! Publish(RoomItem.Spawned(itemId, item))

    log.debug(s"Spawning new item (id: $itemId)")

    item
  }

  override def receive: Receive = initialReceive

  def initialReceive: Receive = {
    case Initialize(room) =>
      context.become(initializedReceive)
      sender() ! Initialized
  }

  def initializedReceive: Receive = {
    case Initialize(_) => sender() ! AlreadyInitialized

    case SpawnItem(itemId, pos) => sender() ! spawnItem(itemId, pos)
  }
}

object ItemDirector {
  case class SpawnItem(itemId: Int, pos: Vector3)

  def props()(mapCoordinator: ActorRef, broadcaster: ActorRef) = Props(classOf[ItemDirector], mapCoordinator, broadcaster)
}