package de.lightwave.rooms.engine.item

import akka.actor.{Actor, ActorRef, Props}
import akka.actor.Actor.Receive

case class ItemReference()

class RoomItem(id: Int, mapCoordinator: ActorRef, itemDirector: ActorRef, broadcaster: ActorRef) extends Actor {
  override def receive: Receive = {
    case _ =>
  }
}

object RoomItem {
  case class Spawned(id: Int, item: ActorRef)

  def props(itemId: Int)(mapCoordinator: ActorRef, itemDirector: ActorRef, broadcaster: ActorRef): Props =
    Props(classOf[RoomItem], itemId, mapCoordinator, itemDirector, broadcaster)
}