package de.lightwave.frontend.io.tcp.protocol.alpha

import akka.util.ByteString
import de.lightwave.frontend.io.tcp.protocol.MessageHeader
import org.scalatest.FunSuite

class AlphaMessageHeaderSpec extends FunSuite {
  test("Create message header from byte string") {
    assert(AlphaMessageHeader.parse(ByteString("@@C@A")) == MessageHeader(1, 1))
  }

  test("Create message header from byte string with packet length smaller than 2") {
    assert(AlphaMessageHeader.parse(ByteString("@@A@A")) == MessageHeader(0, 1))
  }
}
