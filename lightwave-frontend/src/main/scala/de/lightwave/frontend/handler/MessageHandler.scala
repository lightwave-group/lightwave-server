package de.lightwave.frontend.handler

import akka.actor.Actor
import de.lightwave.frontend.handler.MessageHandler.HandleMessage

/**
  * Forwards client messages to handlers of a specific role
  */
abstract class MessageHandler extends Actor {
  override def receive: Receive = {
    case HandleMessage(msg, authenticated) => handleMessage(authenticated)(msg)
  }

  def handleMessage(authenticated: Boolean): Receive
}

object MessageHandler {
  case class HandleMessage(msg: Any, authenticated: Boolean = true)
}