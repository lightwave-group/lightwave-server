package de.lightwave.frontend.handler

import akka.actor.{Actor, ActorLogging, ActorRef, Terminated}
import akka.util.Timeout
import de.lightwave.frontend.handler.RoomHandler.SetEntity
import de.lightwave.rooms.engine.entity.EntityDirector.SpawnEntity
import de.lightwave.rooms.engine.entity.EntityReference
import de.lightwave.services.pubsub.Broadcaster.Subscribe

abstract class RoomHandler(connection: ActorRef, entityReference: Option[EntityReference], roomEngine: ActorRef) extends Actor with ActorLogging {
  import context.dispatcher
  import akka.pattern._
  import scala.concurrent.duration._

  implicit val timeout = Timeout(5.seconds)

  var entity: Option[ActorRef] = None

  protected var errorMessageOnTermination = false
  context watch roomEngine

  override def preStart(): Unit = {
    if (entityReference.isDefined) {
      (roomEngine ? SpawnEntity(entityReference.get)).map {
        case entity: ActorRef => SetEntity(entity)
      } pipeTo self
    }

    // Subscribe to room events
    roomEngine ! Subscribe(self)
  }

  /**
    * Stop entity actor when handler got stopped
    */
  override def postStop(): Unit = if (entity.isDefined) {
    context.stop(entity.get)
  }

  def entityReceive(entity: ActorRef): PartialFunction[Any, Any]

  def handleRoomUserMessage(msg: Any): Unit = entity match {
    case Some(e) => entityReceive(e)(msg)
    case None => log.warning("Room user message cannot be handled without entity")
  }

  override def receive: Receive = {
    case SetEntity(e) =>
      context watch e
      entity = Some(e)

    // Called when entity or engine is stopped
    case Terminated(_) =>
      entity = None
      errorMessageOnTermination = true
      context.stop(self)
  }
}

object RoomHandler {
  case class SetEntity(entity: ActorRef)
}