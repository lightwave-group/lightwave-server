package de.lightwave.frontend.io.tcp.protocol.alpha

import akka.util.ByteString
import de.lightwave.frontend.io.tcp.protocol.{Message, MessageHeader, MessageHeaderParser, MessageParser}

trait AlphaMessage extends Message

object AlphaMessageHeader extends MessageHeaderParser {
  override def headerLength = 5

  override def parse(header: ByteString): MessageHeader = {
    if (header.length < AlphaMessageHeader.headerLength) {
      throw new IllegalArgumentException("Invalid header size.")
    }
    val len = AlphaNumberEncoding.decodeShort(header.slice(0, 3)) - 2
    MessageHeader(if (len <= 0) 0.toShort else len.toShort, AlphaNumberEncoding.decodeShort(header.slice(3, 5)))
  }
}

/**
  * Parsing byte string messages from Shockwave clients to
  * associated message objects
  */
trait AlphaMessageParser[T] extends MessageParser[T] {
  def parse(header: MessageHeader, body: ByteString): T = parse(AlphaMessageReader.from(header, body))
  def parse(reader: AlphaMessageReader): T
}

trait AlphaMessageComposer {
  def init(opCode: Short): AlphaMessageWriter = new AlphaMessageWriter(opCode)
}