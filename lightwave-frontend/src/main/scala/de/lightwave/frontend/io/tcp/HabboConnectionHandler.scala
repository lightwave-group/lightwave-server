package de.lightwave.frontend.io.tcp

import java.net.InetSocketAddress

import akka.actor.{ActorRef, Props}
import de.lightwave.frontend.handler.MessageHandler.HandleMessage
import de.lightwave.frontend.io.tcp.HabboConnectionHandler.{EnterRoom, GetPlayerInformation, SetPlayerInformation}
import de.lightwave.frontend.io.tcp.protocol.{MessageHeaderParser, MessageParserLibrary}
import de.lightwave.players.model.Player
import de.lightwave.rooms.engine.entity.EntityReference

abstract class HabboConnectionHandler(remoteAddress: InetSocketAddress,
                             connection: ActorRef,
                             messageHandler: ActorRef,
                             messageHeaderParser: MessageHeaderParser,
                             messageParserLib: MessageParserLibrary,
                             roomHandlerProps: (ActorRef, Option[EntityReference], ActorRef) => Props)
  extends ConnectionHandler(remoteAddress, connection, messageHeaderParser, messageParserLib) {

  var playerInformation: Option[Player] = None
  var roomHandler: Option[ActorRef] = None

  protected def handleRoomMessage(msg: Any): Unit = roomHandler match {
    case Some(handler) => handler ! msg
    case None => // Room not set
  }

  override def handleMessage(msg: Any): Unit = msg match {
    case _ => messageHandler ! HandleMessage(msg, playerInformation.isDefined)
  }

  override def customReceive: Receive = {
    case SetPlayerInformation(player) => playerInformation = Some(player)
    case GetPlayerInformation => sender() ! playerInformation
    case EnterRoom(engine) => roomHandler = Some(
      context.actorOf(roomHandlerProps(self, Some(EntityReference(1, "Steve")), engine))
    )
  }
}

object HabboConnectionHandler {
  case class SetPlayerInformation(player: Player)
  case class EnterRoom(engine: ActorRef)
  case object GetPlayerInformation
}