import Commons.module
import Commons.service

name    := "lightwave"
version := Commons.Versions.lightwave

scalaVersion in Global := Commons.Versions.scala

lazy val common = module("common")

lazy val players = service("players", "de.lightwave.players.PlayerServiceApp")
  .dependsOn(common)

lazy val rooms = service("rooms", "de.lightwave.rooms.RoomServiceApp")
  .dependsOn(common)

lazy val frontend = module("frontend")
  .dependsOn(rooms, players)

lazy val shockwave = service("shockwave", "de.lightwave.shockwave.ShockwaveServiceApp")
  .dependsOn(common, frontend, rooms, players)

lazy val uber = service("uber", "de.lightwave.uber.UberServiceApp")
  .dependsOn(common, frontend, rooms, players)

val services = Seq(
  rooms,
  players,
  shockwave
)